## KETI_DB_Scripter
- [KETI DB v2.0 link](https://ketimobile-my.sharepoint.com/:w:/g/personal/sens_ketimobile_onmicrosoft_com/ES76_73H-zhLtF9tJvh53dwBcVpNYN5hIYrrQ-w2py6wfA?rtime=mu2yUpC32Eg)

### TODO
- [x] replace weather search API
- [x] test with real KETI DB

### requirements
- pip install tqdm lxml requests
- [VWorld API Key](https://www.vworld.kr/dev/v4dv_geocoderguide2_s002.do) (search location)
- [KMA API Key](https://www.data.go.kr/tcs/dss/selectApiDataDetailView.do?publicDataPk=15059093) (search weather)

### args
- --dataset : dataset location
- --api_key : API key, search location with gps data(default value exist)
- --weather_api_key : API key, search weather information with time and location(default value exist)

### command
```
When KETI DB directory like below. 

DATASET_ROOT
├── Camera
│   ├── CAM_BACK
│   │   ├── KETI_CAM_BACK_000000.jpg
│   │   ├── ...
│   │   ├── KETI_CAM_BACK_n.jpg
│   ├── CAM_BACK_LEFT
│   ├── ...
│   ├── CAM_FRONT_LEFT
│   ├── CAM_FRONT_RIGHT
├── GPS
│   ├── KETI_GPS_000000.txt
│   ├── ...
│   ├── KETI_GPS_n.txt
├── IMU
│   ├── KETI_IMU_000000.txt
│   ├── ...
│   ├── KETI_IMU_n.txt
├── Lidar
│   ├── KETI_LIDAR_000000.bin
│   ├── ...
│   ├── KETI_LIDAR_n.bin
├── Radar
│   ├── RADAR_BACK_LEFT
│   │   ├── KETI_RADAR_BACK_LEFT_000000.bin
│   │   ├── ...
│   │   ├── KETI_RADAR_BACK_LEFT_n.bin
│   ├── ...
│   ├── RADAR_FRONT_RIGHT
├── Sync
│   ├── KETI_Sync_000000.txt
│   ├── ...
│   ├── KETI_Sync_n.txt
├── VEHICLE_INFO
│   ├── KETI_VEHICLE_INFO_000000.txt
│   ├── ...
│   ├── KETI_VEHICLE_INFO_n.txt

Excute scripter code with arguments,
Then Description directory and files are made like below.
ex) python KETI_DB_descriptor.py --dataset {location DATASET_ROOT}

DATASET_ROOT
├── Camera
├── GPS
├── IMU
├── Lidar
├── Radar
├── Sync
├── VEHICLE_INFO
├── Description
│   ├── KETI_DATA
│   │   ├── KETI_Data_000000.json
│   │   ├── ...
│   │   ├── KETI_Data_n.json
│   ├── KETI_Vehicle.json
│   ├── KETI_World.json
│   ├── Annotation
│   │   ├── 2D
│   │   ├── 3D
│   │   ├── PointCloud_3D
│   ├── Segmentation
│   │   ├── Infomations
│   │   ├── labels
│   │   ├── KETI_Segmentation_info.json
```
