import os
import json_template as info
import json
import argparse
from pathlib import Path
import requests
from lxml.html import fromstring
import time
from tqdm import tqdm


def parse_config():
    parser = argparse.ArgumentParser(description='arg parser')
    parser.add_argument('--dataset_path', type=str, default='./', help='specify the dataset path location')
    parser.add_argument('--api_key', type=str, default='74DC9AC4-8559-3B0A-A9BB-D24A6F0F4462', help='specify the API KEY to find location')
    parser.add_argument('--weather_api_key', type=str, default='wH%2BNNv3lm1UXCzrn6Q4cxyKzVXAKSZyF9WOQi%2BM6czn6unbfWWn8jqpXGIvsNyzvaeMhsgv3mBokWhi%2Bm59WlA%3D%3D'
                        , help='specify the API KEY to search weather')

    args = parser.parse_args()

    return args


def main():
    # TODO: separate as a function
    # parse argument
    args = parse_config()

    # data path, bagfile name split
    root = Path(args.dataset_path)
    bagfile_name = str(args.dataset_path).split('/')[-2]

    # sensor data dir path split
    sync_path = root / 'Sync'

    # make Description dir
    if not os.path.isdir(f'{root}/Description/KETI_Data'):
        os.makedirs(os.path.join(f'{root}/Description/KETI_Data'))

    condition_dict = {
        'Clear': 'Dry',
        'Snow': 'Snow on the floor',
        'Rain': 'Wet road',
        'Sleet': 'Icy road'
    }

    season_dict = {
        'Spring': [3, 4, 5],
        'Summer': [6, 7, 8],
        'Fall': [9, 10, 11],
        'Winter': [12, 1, 2]
    }

    # KETI_Data dict ===========================================================================================
    sync_list = sorted(os.listdir(sync_path))

    start_time, time_val = '', ''

    keti_data = info.KETI_Data.copy()

    location_list = []
    road_list = []
    season_list = []
    time_list = []
    traffic_list = []
    weather_list = []
    weather_error_list = []
    condition_list = []

    idx = 0

    for sync_file in tqdm(sync_list):
        # write file location dict
        f = open(f'{root}/Sync/{sync_file}')
        while True:
            filename = f.readline().rstrip()
            if not filename:
                break
            keti_data['Data'][filename.split('/')[-2]] = filename

            if 'GPS' in filename:
                gps_info = open(f'{root}{filename[len(bagfile_name) + 1:]}').readline().split()
                nano_sec = gps_info[0]
                time_val = time.localtime(int(nano_sec) / (10**9))
                if not start_time:
                    start_time = time_val
                hour_val = time_val.tm_hour

                if 10 < hour_val < 16:
                    time_str = 'Day'
                elif 16 < hour_val < 20:
                    time_str = 'Evening twilight'
                elif 20 < hour_val < 24:
                    time_str = 'Night'
                elif 4 < hour_val < 6:
                    time_str = 'Morning Twilight'
                else:
                    time_str = 'None'

                season = 'None'
                for k, v in season_dict.items():
                    if time_val.tm_mon in v:
                        season = k
                        break

                lat, lon = gps_info[1], gps_info[2]
                url = 'http://api.vworld.kr/req/address?service=address&request=getAddress&version=2.0&crs=epsg:4326&point=' \
                      + lon + ',' + lat + '&format=json&type=parcel&zipcode=true&simple=false&key=' + args.api_key
                response = requests.get(url)
                data = response.json()

                location = data['response']['result'][0]['text'] if data['response']['status'] == 'OK' else 'NOT_FOUND'

                loc1 = data['response']['result'][0]['structure']['level1']
                loc2 = data['response']['result'][0]['structure']['level2']

                loc1 = str(loc1).replace('광역시', '')
                loc2 = str(loc2).replace('시', '')

                if loc1 in ('경기도', '서울특별시'):
                    loc1 = '서울'

                if loc1 not in info.weather_loc_dict:
                    loc1 = loc2
                    if loc1 not in info.weather_loc_dict:
                        print(f'{loc1} location code is not found')
                        break

                # TODO: change openAPI
                url = 'http://apis.data.go.kr/1360000/AsosDalyInfoService/getWthrDataList' \
                      '?serviceKey=' + args.weather_api_key + '&numOfRows=10&pageNo=1&dataType=JSON&dataCd=ASOS&dateCd=DAY&startDt=' \
                      + f'{time_val.tm_year:04d}{time_val.tm_mon:02d}{time_val.tm_mday:02d}' \
                      + '&endDt=' + f'{time_val.tm_year:04d}{time_val.tm_mon:02d}{time_val.tm_mday:02d}' + '&stnIds=' + \
                      info.weather_loc_dict[loc1]
                response = requests.get(url)
                if response.status_code == 200:
                    weather = response.json()

                    if weather is not None:
                        rain_fall = float(0 if weather['response']['body']['items']['item'][0]['sumRn'] == ''
                                          else weather['response']['body']['items']['item'][0]['sumRn'])
                        snow_fall = float(0 if weather['response']['body']['items']['item'][0]['ddMes'] == ''
                                          else weather['response']['body']['items']['item'][0]['ddMes'])

                        if rain_fall < 1 and snow_fall < 1:
                            weather = 'Clear'
                        else:
                            weather = 'Rain' if rain_fall > snow_fall else 'Snow'

                        if location != 'NOT_FOUND':
                            location = location.replace(' ', '+')

                            if weather != 'Clear':
                                if rain_fall <= 10 or snow_fall <= 10:
                                    weather += '_1'
                                elif rain_fall <= 20 or snow_fall <= 20:
                                    weather += '_2'
                                elif 20 < rain_fall or 20 < snow_fall:
                                    weather += '_3'
                else:
                    weather = 'None'
                    weather_error_list.append(idx)

                # this code is complete to translate kor location to en.
                url = 'https://search.naver.com/search.naver?where=nexearch&sm=top_hty&fbm=1&ie=utf8&query=' + location + '+영어주소'
                res = requests.get(url)
                parser = fromstring(res.text)
                en_loc = parser.xpath('//*[@id="ds_result"]/div/table/tbody/tr/td[1]/dl/dd[2]/strong')

                keti_data['Timestamp'] = int(gps_info[0]) / 10 ** 9
                keti_data['Gps']['latitude'] = gps_info[1]
                keti_data['Gps']['longitude'] = gps_info[2]
                keti_data['Gps']['height'] = gps_info[3]
                keti_data['Gps']['roll'] = gps_info[4]
                keti_data['Gps']['pitch'] = gps_info[5]
                keti_data['Gps']['yaw'] = gps_info[6]

                keti_data['Scene']['Location'] = '_'.join(str(en_loc[0].text_content()).replace(',', '').split(' ')[-5:])
                keti_data['Scene']['Road'] = condition_dict[weather] if weather in condition_dict else 'None'
                keti_data['Scene']['Season'] = season
                keti_data['Scene']['Time'] = time_str
                keti_data['Scene']['Weather'] = weather

            if 'IMU' in filename:
                imu_info = open(f'{root}{filename[len(bagfile_name) + 1:]}').readline().split()
                keti_data['Imu']['linear_x'] = imu_info[1]
                keti_data['Imu']['linear_y'] = imu_info[2]
                keti_data['Imu']['linear_z'] = imu_info[3]
                keti_data['Imu']['angular_x'] = imu_info[4]
                keti_data['Imu']['angular_y'] = imu_info[5]
                keti_data['Imu']['angular_z'] = imu_info[6]

            if 'Vehicle_info' in filename:
                vinfo = open(f'{root}{filename[len(bagfile_name) + 1:]}').readline().split()
                keti_data['Accele'] = vinfo[0]
                keti_data['Break'] = vinfo[1]
                keti_data['Steering'] = vinfo[2]

            # write keti_data file index into annotation, segmentation json file
            if os.path.isfile(os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/2D{filename[len(bagfile_name) + 1:]}')[0] + '.json'):
                with open(os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/2D{filename[len(bagfile_name) + 1:]}')[0] + '.json', "r") as anno_2d_f:
                    anno_2d_data = json.load(anno_2d_f)
                    anno_2d_data['Filename'] = f'{bagfile_name}/Description/KETI_Data/keti_data_{idx:06d}.json'
                with open (os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/2D{filename[len(bagfile_name) + 1:]}')[0] + '.json', "w") as after_anno_2d_f:
                    json.dump(anno_2d_data, after_anno_2d_f, ensure_ascii=False, indent=4)

            if os.path.isfile(os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/3D{filename[len(bagfile_name) + 1:]}')[0] + '.json'):
                with open(os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/3D{filename[len(bagfile_name) + 1:]}')[0] + '.json', "r") as anno_3d_f:
                    anno_3d_data = json.load(anno_3d_f)
                    anno_3d_data['Filename'] = f'{bagfile_name}/Description/KETI_Data/keti_data_{idx:06d}.json'
                with open (os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/3D{filename[len(bagfile_name) + 1:]}')[0] + '.json', "w") as after_anno_3d_f:
                    json.dump(anno_3d_data, after_anno_3d_f, ensure_ascii=False, indent=4)
                    
            if os.path.isfile(os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/PointCloud_3D{filename[len(bagfile_name) + 1:]}')[0] + '.json'):
                with open(os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/PointCloud_3D{filename[len(bagfile_name) + 1:]}')[0] + '.json', "r") as anno_pcd_f:
                    anno_pcd_data = json.load(anno_pcd_f)
                    anno_pcd_data['Filename'] = f'{bagfile_name}/Description/KETI_Data/keti_data_{idx:06d}.json'
                with open (os.path.splitext(f'{root}/Description/KETI_Annotation/Detection/PointCloud_3D{filename[len(bagfile_name) + 1:]}')[0] + '.json', "w") as after_anno_pcd_f:
                    json.dump(anno_pcd_data, after_anno_pcd_f, ensure_ascii=False, indent=4)
                    
            if os.path.isfile(os.path.splitext(f'{root}/Description/KETI_Annotation/Segmentation/infomations{filename[len(bagfile_name) + 1:]}')[0] + '.json'):
                with open(os.path.splitext(f'{root}/Description/KETI_Annotation/Segmentation/infomations{filename[len(bagfile_name) + 1:]}')[0] + '.json', "r") as anno_seg_f:
                    anno_seg_data = json.load(anno_seg_f)
                    anno_seg_data['Filename'] = f'{bagfile_name}/Description/KETI_Data/keti_data_{idx:06d}.json'
                with open (os.path.splitext(f'{root}/Description/KETI_Annotation/Segmentation/infomations{filename[len(bagfile_name) + 1:]}')[0] + '.json', "w") as after_anno_seg_f:
                    json.dump(anno_seg_data, after_anno_seg_f, ensure_ascii=False, indent=4)

        keti_data['Scene']['Traffic'] = 'None'
        keti_data['Scene']['Condition'] = 'None'

        location_list.append(keti_data['Scene']['Location']) \
            if keti_data['Scene']['Location'] not in location_list else 'None'
        road_list.append(keti_data['Scene']['Road']) \
            if keti_data['Scene']['Road'] not in road_list else 'None'
        season_list.append(keti_data['Scene']['Season']) \
            if keti_data['Scene']['Season'] not in season_list else 'None'
        time_list.append(keti_data['Scene']['Time']) \
            if keti_data['Scene']['Time'] not in time_list else 'None'
        traffic_list.append(keti_data['Scene']['Traffic']) \
            if keti_data['Scene']['Traffic'] not in traffic_list else 'None'
        weather_list.append(keti_data['Scene']['Weather']) \
            if keti_data['Scene']['Weather'] not in weather_list else 'None'
        condition_list.append(keti_data['Scene']['Condition']) \
            if keti_data['Scene']['Condition'] not in condition_list else 'None'

        with open(f'{root}/Description/KETI_Data/keti_data_{idx:06d}.json', "w") as json_file:
            json.dump(keti_data, json_file, indent=4)

        idx += 1

    # KETI_World ===============================================================================================
    keti_world = info.KETI_World.copy()
    sequence = False

    now = time.localtime()
    keti_world['Name'] = bagfile_name
    keti_world['CreatedTime'] = f'{now.tm_year}-{now.tm_mon:02d}-{now.tm_mday:02d} {now.tm_hour:02d}:{now.tm_min:02d}:{now.tm_sec:02d} '
    keti_world['Sequence'] = sequence
    keti_world['Scene']['Location'] = location_list
    keti_world['Scene']['Road'] = condition_list
    keti_world['Scene']['Period'] = f'{start_time.tm_year}_{start_time.tm_mon:02d}_{start_time.tm_mday:02d} ' \
                                    f'~ {time_val.tm_year}_{time_val.tm_mon:02d}_{time_val.tm_mday:02d}'
    keti_world['Scene']['Season'] = season_list
    keti_world['Scene']['Time'] = time_list
    keti_world['Scene']['Traffic'] = traffic_list
    keti_world['Scene']['Weather'] = weather_list
    keti_world['Scene']['Condition'] = condition_list

    with open(f'{root}/Description/KETI_World.json', "w") as json_file:
        json.dump(keti_world, json_file, ensure_ascii=False, indent=4)

    # Vehicle_info ============================================================================================
    vehicle_info = {}
    vehicle_info['Camera_info'] = info.Camera_info.copy()
    vehicle_info['Lidar_info'] = info.Lidar_info.copy()
    # vehicle_info['Radar_info'] = info.Radar_info.copy()
    vehicle_info['Image_data_info'] = info.Image_data_info.copy()
    vehicle_info['Points_data_info'] = info.Points_data_info.copy()

    with open(f'{root}/Description/Vehicle_info.json', "w") as json_file:
        json.dump(vehicle_info, json_file, ensure_ascii=False, indent=4)

    if weather_error_list:
        print(f'Weather error : {weather_error_list}')
    print('Description Generation Completed.')


if __name__ == '__main__':
    main()
